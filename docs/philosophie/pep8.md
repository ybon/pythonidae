# pep8

*«Code is read much more often than it is written»*, Guido Van Rossum.

## PEP?

Python Enhancement Proposal: un document exposant toute amélioration substantielle
de python avant son implémentation et son intégration dans le code.

Voir https://www.python.org/dev/peps/.

## PEP8?

La PEP8 décrit les conventions pour écrire le code python.

### Quelques règles de base

- indentation de 4 espaces (pas de tab)

## Ressources

- [http://pep8.org/](http://pep8.org/)
