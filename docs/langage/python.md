# Python

## Bytecode

Python est un langage dit interprété, à la différence des langages dits «compilés».
C'est-à-dire que le code est chargé et transformé en bytecode à chaque lancement de python.

Par défaut, quand on parle de python, on sous-entend l'interpréteur CPython, mais
il en existe d'autres: pypy, jython…

## Load time vs run time

Il est parfois utile de distinguer ces deux étapes pour débugguer ou mieux comprendre
le fonctionnement d'un décorateur.


## Espaces de noms

local vs global
