# Patterns idiomatiques

## Décorateurs

Une fonction qui altère une autre fonction (ou méthode).

```python
def decorator(func):
    # Ce block est exécuté au load time.
    # Ici on peut faire n'importe quoi, y compris renvoyer une autre fonction
    # par exemple une fonction qui validerait des droits avant d'appeler la
    # fonction originale.
    def autre_func(xxx):
        # C'est cette fonction qui va être appelée à la place de func.
        # On peut par exemple, valider des droits ou valider des paramètres:
        if xxx is None:
            raise ValueError("xxx ne doit pas être vide")
        return func(xxx)
    return autre_func


@decorator
def ma_fonction(xxx):
    pass
```

Un décorateur peut prendre des arguments, c'est comme un double décorateur:

```python
def decorator(niveau_de_droits):
    def load_time_decorator(func):  # Maintenant on retrouve notre décorateur simple.
        # Ici on peut faire quelque chose avec func et niveau_de_droits
        return func
    return load_time_decorator


@decorator(niveau_de_droits="admin")
def my_func(xxx):
    pass
```

## Générateurs

Un itérable lazy.

```python
def mygen():
    yield "un"
    yield "deux"
    yield "trois"


for i, out in enumerate(mygen()):
    print(i, out)
0 "un"
1 "deux"
2 "trois"
```

## Context managers

Pour gérer l'ouverture et la fermeture d'un contexte (par exemple ouvrir un fichier
ou gérer une transaction en base de données).

```python
from pathlib import Path

with Path('path/to/file').open() as f:
    # Faire quelque avec f
    print(f.read())
# Ici le fichier a été automatiquement fermé (même si une erreur a été levée
# dans le bloc du context manager.
```


## list/dict comprehension

```python
mylist = [element for element in other_iterable]
mydict = {k: v for k, v in other_iterable}
```
