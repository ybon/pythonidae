# Modèle de données

Les types de base.

## Les types simples

- `int`: un nombre

        x = 12

- `float`: un nombre décimal

        x = 3.1415

- `bool`: un booléen

        x = False
        y = not x
        assert y


## Séquences et sets

- `str`: une chaîne de caractères

        x = "une chaîne"

- `bytes`: une séquence d'octets

        x = b"des octets"

- `list`: une liste d'éléments arbitraires

        x = ["un", 2, b"trois"]
        x.append(4.0)
        print(x)
        ["un", 2, b"trois", 4.0]

- `tuple`: une séquence immutable

        x = ("un", 2, b"trois")
        # Attention, ceci n'est pas une tuple, mais une chaîne:
        x = ("un")
        # Pour écrire un tuple d'un seul élément, il faut mettre la virgule
        x = ("un",)

- `set`: le `set` n'est pas une séquence à proprement parler, parce qu'il n'est
  pas ordonné

        x = {"un", 2, b"trois"}
        print(x)
        {2, b"trois", "un"}
        assert "un" in x
        for y in x:
           print(y)


## Mappings

- `dict`, ou dictionnaire, un ensemble de clés/valeurs

        x = {"clé": "valeur", "autreclé": "autrevaleur"}
        # Attention, pour itérer sur les clés et valeurs, il faut utiliser .items()
        for key, value in x.items():
            print(key, value)
        # Ceci ne va itérer sur les clés:
        for item in x:
            print(item)


Mais aussi: long, complex, bytearray, frozenset…

Et aussi: function, module, None, type, object…
