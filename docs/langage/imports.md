# Les imports

```python
# Rend le module mymodule disponible à travers la variable mymodule.
import mymodule
# Rend le module mymomdule.mysubmodule disponible à travers la variable mysubmodule.
from mymodule import mysubmodule
# On navigue dans l'arborescence via des "." (pas des /)
from mymodule.submodule import myfunction
```

Les modules importables sont ceux qui sont définis dans le PYTHONPATH.

## PYTHONPATH

`PYTHONPATH` est une [variable système](https://docs.python.org/3/using/cmdline.html#envvar-PYTHONPATH)
définissant la liste de chemins sur la machine où python va
[chercher](https://docs.python.org/3/tutorial/modules.html#the-module-search-path).

Pour voir sa valeur:

```python
import sys
print(sys.path)
```

## Package vs module

Un [module](https://docs.python.org/3/tutorial/modules.html) est tout simplement
un fichier python, c'est-à-dire un fichier finissant par `.py`.
Un [package](https://docs.python.org/3/tutorial/modules.html#packages) est un
répertoire contenant au minimum un fichier `__init__.py`.


## Imports absolus ou relatifs

```python3
# Un import absolu
import csv
import mymodule
# Un import relatif
from . import mysubmodule
from .submodule import myfunction
from ...topmodule import myfunction
```

## Imports circulaires

Un module A ne peut pas importer un module B qui importe lui-même le module A.
De même si un module A import un module B qui importe un module C qui importe un
module D… qui importe un module A.

C'est souvent un signe d'un problème de conception. La solution est souvent de
déplacer une partie du code du module A vers un module AA, qui va lui-même être
une dépendance de A et B.

En dernier recours, on peut faire un import *at runtime*, c'est-à-dire directement
dans le code qui va s'exécuter quand le code python sera appelé, par exemple
dans le corps d'une fonction:

```python3
def myfunction():
    # Workaround circular imports
    from moduleb import something
    something()
```


## Ressources

- http://sametmax.com/les-imports-en-python/
