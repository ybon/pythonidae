# Gérer les exceptions

Une exception est un signal notifiant le comportement anormal d'un code. Elle
interrompt le déroulement normal du code.

# Types d'exceptions

Les exceptions sont typées, voir https://docs.python.org/3/library/exceptions.html

Il est aussi possible de créer ses propres types.

```python
class MonException(Exception):
    pass
```

## Lever une exception

```python
raise ValueError("Description du problème")
```

## Attraper une exception

```python
try:
    # Un code qui peut lever ValueError
    execute_code()
except ValueError:
    pass
    # Comportement en cas d'erreur
# Exécuter du code seulement si aucun exception n'est levée
try:
    # code
except Exception:
    # Gérer l'erreur.
else:
    # Il n'y a pas eu d'erreur
# Exécuter du code dans tous les cas, même si une erreur est levée et a interrompu
def division(a, b):
    try:
        result = a / b
    except ZeroDivisionError:
        print("Erreur")
        # Transmet l'erreur et interrompt le programme
        raise
    else:
        print("Pas d'erreur")
        return result
    finally:
        print("Dans tous les cas")
    print("Jamais appelé")
# Pas d'erreur
division(2, 4)
"Pas d'erreur"
"Dans tous les cas"
0.5
# Division par 0.
division(2, 0)
"Erreur"
"Dans tous les cas"
[…]
ZeroDivisionError: division by zero
```

Quand on attrape une exception, on attrape aussi tous les exceptions filles.

```python
# Attraper plusieurs types en même temps
try:
    # quelque chose
except (ValueError, TypeError):
    pass
# Attraper plusieurs types mais exécuter des code différents
try:
    # quelque chose
except ValueError:
    # En cas de ValueError
except TypeError:
    # En cas de TypeError
```

## Utiliser l'erreur

```python
try:
    # quelque chose
except ValueError as err:
    print(err)
```

## Bubbling

Une erreur remonte dans la stack tant que personne ne l'a pas attrapée, et
in fine va générer une erreur de l'interpréteur.


## Ressources

- https://docs.python.org/3/library/exceptions.html
- http://sametmax.com/gestion-des-erreurs-en-python/
- http://sametmax.com/tableau-de-reference-des-exceptions-en-python/
