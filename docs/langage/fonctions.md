# Les fonctions

Définition d'une fonction simple:

```python3
def say_hi(name, scream=False):
    out = f"Hi {name}!"
    if scream:
        out = out.upper()
    print(out)
```

- `name` est un *argument positionnel*, il est obligatoire.
- `scream` est un *argument nommé*, il est optionnel, sa valeur par défaut est
`False`.

```python3
say_hi("Mark")
"Hi Mark!"
# On peut passer l'argument via son nom
say_hi(name="Mark")
"Hi Mark!"
# Mais on ne peut pas l'omettre
say_hi()
TypeError: say_hi() missing 1 required positional argument: 'name'
# On peut passer une valeur pour scream via son nom:
say_hi("Mark", scream=True)
"HI MARK!"
# Ou via sa position
say_hi("Mark", scream=True)
"HI MARK!"
```

## Arguments dynamiques
Il est possible d'exploser une liste ou un dictionnaire pour passer les arguments
dynamiquements:
```python3
# Une liste
arguments = ["Mark", True]
say_hi(*arguments)
"HI MARK!"
# De même avec un dictionnaire
arguments = {"name": "Mark", "screem": "True"}
say_hi(**arguments)
"HI MARK!"
# Les deux en même temps:
positionnels = ["Mark"]
nommes = {"scream": True}
say_hi(*positionnels, **nommes)
"HI MARK!"
```

Il est possible d'indiquer des arguments positionnels ou nommés optionnels dans
la définition de la fonction:

```python3
def say_hi(name, *other_names):
    out = f"Hi {name}"
    for name in other_names:
        out += f" {name}"
    print(out)
```
`name` est toujours l'unique argument obligatoire:

```python3
say_hi("Mark")
"Hi Mark"
```

On peut passer des arguments complémentaires:

```python3
say_hi("Mark", "Freuder", "Knopfler")
"Hi Mark Freuder Knopfler"
# On peut toujours exploser une liste:
arguments = ["Mark", "Freuder", "Knopfler"]
say_hi(*arguments)
"Hi Mark Freuder Knopfler"
# De même pour une partie seulement des arguments
others = ["Freuder", "Knopfler"]
say_hi("Mark", *others)
"Hi Mark Freuder Knopfler"
```

De même avec des arguments nommés:

```python3
def say_hi(**names):
    for first_name, last_name in names.items():
        print(f"Hi {first_name} {last_name}")
say_hi(Mark="Knopfler", David="Knopfler")
"Hi Mark Knopfler"
"Hi David Knopfler"
```

```python
def say_hi(name, *, lastname):
    pass
# Va générer une erreur
say_hi("Mark", "Knopfler")
# OK
say_hi("Mark", lastname="Knopfler")
```
