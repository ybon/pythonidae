# Programmation orientée objet

Souvent appelée *OOP*, pour *Object-Oriented Programing*.

## Définition

La [programmation orientée objet](https://fr.wikipedia.org/wiki/Programmation_orient%C3%A9e_objet)
consiste à attacher des propriétés (des *attributs*) et des comportements (des
*méthodes*) à des objets.

Par exemple, si on a un objet `Person`, cet objet pourrait avoir les propriétés
`age`, `height`… et les comportements `walk`, `laugh`…

Dans le cas de l'OOP, le code va construire des objets et les manipuler.

On l'oppose à la [programmation procédurale](https://fr.wikipedia.org/wiki/Programmation_proc%C3%A9durale)
qui consiste à enchaîner les instructions, comme dans une recette.


## Syntaxe

```python
class Cat:
    # Attribut de classe: valeur partagée par toutes les instances.
    eat_birds = True

    def speak(self):
        return "Meow meow"

cat = Cat()
print(cat.speak())
"Meow meow"
isinstance(cat, Cat)
True
```

On appelle *classe* la définition de l'objet (ici `Cat`), et *instance* un objet
en particulier (ici `cat`).

*Note: on utilise le CamelCase pour les noms de classe en python: `class BigCat:`.*

### Propriétés d'instance

```python
class Cat:

    def __init__(self, name):
        self.name = name

    def introduce(self):
        return f"I'm a {self.__class__.__name__}, and my name is {self.name}"


cat = Cat("Minou")
print(cat.introduce())
"I'm a Cat, and my name is Minou"
```

### Héritage

```python
class Animal:
    legs = None
    speed = 1

    def walk(self):
        return self.legs * self.speed


class Cat(Animal):
    legs = 4


class Bird(Animal):
    legs = 2
    speed = .5


class Snake(Animal):
    legs = 0
    speed = .3

    def walk(self):
        return self.speed


class Kangaroo(Animal):
    legs = 2
    speed = 2

    def walk(self):
        speed = super().walk()
        return speed * 2  # Vary fast.
```

#### Héritage multiple

```python
# On peut hériter de plusieurs classes parentes
class A:
    def __init__(self):
        print("Hi, I'm a A")

class B:
    def __init__(self):
        print("Hi, I'm a B")

class C(A, B):
    pass

class D(B, A):
    pass

c = C()
"Hi, I'm a A"
d = D()
"Hi, I'm a B"
# Si chacune appelle son parent
class A:
    def __init__(self):
        print("Hi, I'm a A")
        super().__init__()

class B:
    def __init__(self):
        print("Hi, I'm a B")
        super().__init__()

class C(A, B):
    pass

class D(B, A):
    pass

c = C()
"Hi, I'm a A"
"Hi, I'm a B"
d = D()
"Hi, I'm a B"
"Hi, I'm a A"
C.mro()
[C, A, B, object]
D.mro()
[D, A, B, object]
```

### classmethod

```python
class Cat:

    @classmethod
    def speak(cls):
        # Une méthode qui n'est pas spécifique à une instance.
        return "Meow"

    @classmethod
    def create(cls, name, age):
        # ou une méthode qu'on appelle sur la classe directement.
        # On peut par exemple ici vérifier les données
        return cls(name)

cat = Cat.create("Minou", 3)
```

### property

```python
class Cat:

    def __init__(self, age):
        self.age = age

    @property
    def food(self):
        # Comme un attribut (age), mais dynamique.
        if self.age < 1:
            return "milk"
        else:
            return "biscuit"

cat = Cat(3)
cat.food
"biscuit"

# Peut être utilisé pour contrôler la donnée
class Cat:

    def __init__(self, age):
        self.age = age
        self._food = None

    @property
    def food(self):
        return self._food

    @food.setter
    def food(self, value):
        if not value.is_organic:
            raise ValueError("Cat can't eat non organic food")
        self._food = value

    @food.deleter
    def food(self):
        if not self.is_replete:
            raise ValueError("Can't remove food of an hungry cat")
        self._food = None
```

### Méthodes spéciales


On en a déjà vu une: `__init__`.

Elles servent à permettre à des objets de se comporter comme des objets Python.

Voici les principales:

- `__str__`: pour contrôler le `str(obj)` ou le `f"{obj}"`
- `__repr__`: pour contrôler le `print([obj])`
- `__call__`: pour faire de l'objet un callable: `obj(param, param)`
- `__iter__`: pour faire de l'objet un iterable: `for x in obj:`
- `__len__`: pour pouvoir faire `len(obj):`
- `__getitem__`, `__setitem__`, `__delitem__`…: pour émuler un conteneur: `obj[item]`
- `__format__`: pour contrôler le formattage de chaîne `f"{obj}"`
- `__bool__`: pour contrôler l'évaluation booléenne: `if obj:`
- `__lt__`, `__gt__`, `__le__`…: pour contrôler la comparaison (*rich comparison*):
  `if myobj < myotherobj`
- `__add__`, `__sub__`…: pour émuler les types numériques: `myobj + myobj`

- `__getattr__`, `__setattr__`: pour contrôler dynamiquement l'accès aux attributs

Voir la liste complète: https://docs.python.org/3/reference/datamodel.html#special-method-names

### staticmethod


```python
class Cat:

    @staticmethod
    def foo(param):
        # Ici, pas de self, ni de cls
        # C'est juste une fonction attachée à la classe. En général, on l'utilise
        # pour ranger les functions logiquement.
        return "foo"
```

### getattr/hasattr/setattr

```python
hasattr(cat, "eat_birds")
True
hasattr(cat, "speak")
True
hasattr(cat, "sing")
False
getattr(cat, "eat_birds")
True
getattr(cat, "eat_salade")
AttributeError: 'Cat' object has no attribute 'eat_salad'
getattr(cat, "eat_salade", "default")
"default"
setattr(cat, "eat_salad", False)
cat.eat_salad
False
```
