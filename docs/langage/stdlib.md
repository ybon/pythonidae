# Librairie standard

On appelle la librairie standard, ou stdlib, les modules founis nativement avec
Python.

## Modules remarquables

- **argparse** parser les arguments d'une ligne de commande
- **csv** lecture/écriture de csv
- **collections** containeurs de données avancés (Counter, defaultdict…)
- **email** gestion d'envoi et réception d'emails
- **enum** enums en Python
- **json** lecture/écriture de JSON
- **logging** gestion des logs
- **multiprocessing**, **concurrent.futures** parallélisation haut niveau
- **pathlib** manipulation de fichiers et répertoires
- **pickle**, **marshal** sérialisation d'objects Python
- **sqlite3** driver SQLite
- **subprocess** gestion de sous-process
- **zlib**, **zipfile**, **gzip**… algorithmes de compression

## Ressources

- https://docs.python.org/3/library/index.html
