# Structures de contrôle

Elles décrivent l’enchaînement des instructions. Elles permettent des traitements séquentiels, conditionnels ou répétitifs (itératifs).


## if / elif / else

```python3
# Expression booléenne.
if something is True:
    do_something_else()
# On peut combiner les expressions.
if something and something_else:
    do_it()
# On peut enchaîner les conditions exclusives:
if something:
    do_it()
elif something_else:
    do_whatever()
else:
    # Si aucune condition n'est vraie
    do_default()
```

## while / else

```python3
# Répéter un block tant qu'une condition est vraie.
while something:
 do_it_again()
 if good:
    # Arrêter la boucle.
    break
```

Exécuter une instruction si la boucle n'est pas interrompue:

```python3
while value < threshold:
    if not process_acceptable_value(value):
        # something went wrong, exit the loop; don't pass go, don't collect 200
        break
    value = update(value)
else:
    # value >= threshold; pass go, collect 200
    handle_threshold_reached()
```

## for / else

Boucler sur chaque élément d'un ensemble (un *itérable*).


**iterable**: en python, une variable est dite "itérable" si on peut boucler dessus, c'est-à-dire
concrètement qu'elle a définie la méthode spéciale `__iter__`.

Sont par exemple itérables: les listes, les dictionnaires, les chaînes, les générateurs


```python3
mylist = [1, 2, 3, 4]
for element in mylist:
    do_something_with(element)
    if test(element):
        # Arrêter la boucle.
        break
```

Exécuter une instruction si la boucle n'est pas interrompue:

```python3
for i in [1, 2, 3, 4]:
    pass
else:
    print("Cette instruction va être exécutée")
for i in [1, 2, 3, 4]:
    if i / 2 % 2 == 0:
        break
else:
    print("Cette instruction ne va pas être exécutée")
```
