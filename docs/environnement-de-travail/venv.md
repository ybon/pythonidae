# Virtual environments

Also called *virtualenv*, or in short *venv*.

## Why use a virtualenv?

- to keep system out of your project dependencies
- to be able to have different versions of a dependency in various project

## Anatomy

A venv is a specific directory used by python to install it's packages.

*Note: this directory should not be versioned in your vcs.*


## Usage

### Create a venv

It's up to you to decide where to store the venv directory. If you use a wrapper,
it will certainly have defined it for you (eg. `virtualenvwrapper` will store all
venv in `~/.virtualenvs`).

```bash
python -m venv path/to/venv
# Activate it
source path/to/venv/bin/activate
```

If you want a specific version of python to be used in this venv, use it to create
it:

```bash
python3.8 -m venv path/to/venv
```


## Resources

- [https://docs.python.org/3/library/venv.html](https://docs.python.org/3/library/venv.html)
- [https://chriswarrick.com/blog/2018/09/04/python-virtual-environments/](https://chriswarrick.com/blog/2018/09/04/python-virtual-environments/)
