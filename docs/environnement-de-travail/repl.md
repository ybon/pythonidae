# REPL

C'est le shell intéractif Python.

REPL == Read, Eval, Print and Loop

Pour le lancer, taper simple `python` dans un terminal:

```
$ python
Python 3.7.3 (default, Mar 26 2019, 21:43:19)
[GCC 8.2.1 20181127] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

Versions étendues: ipython, bpython, ptpython
